package utilities;

public class UtilityFunctions {


    public final static String email = randomEmail();

    public static long currentEpochTime() {
        long epochTime = System.currentTimeMillis();
        return epochTime;

    }

    public static String randomEmail() {
        String email = "sirajcomp_" + currentEpochTime() + "@mailinator.com";
        return email;

    }

}
