package utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import dataProviders.Credentials_ConfigFileReader;
import managers.FileReaderManager;

public class MysqlDbConnection {

    Credentials_ConfigFileReader credentials_ConfigFileReader = new Credentials_ConfigFileReader();

    public int getValidationCode(String email) {

        String driverClass, usrName, pwd, url;
        int valCode = 0;

        driverClass = "com.mysql.jdbc.Driver";

        usrName = FileReaderManager.getInstance().getCredentials_ConfigReader().getDB_UserName();
        pwd = FileReaderManager.getInstance().getCredentials_ConfigReader().getDB_Password();
        url = FileReaderManager.getInstance().getCredentials_ConfigReader().getDB_URL();

        try {
            Class.forName(driverClass);

            Connection con = (Connection) DriverManager.getConnection(url, usrName, pwd);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(
                    "SELECT CODE FROM VALIDATION WHERE USER_ID = (SELECT ID from USER WHERE email = '" + email + "')");
            while (rs.next())
                valCode = rs.getInt(1);
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return valCode;
    }
}
