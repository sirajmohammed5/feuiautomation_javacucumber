package managers;

import dataProviders.Credentials_ConfigFileReader;
import dataProviders.URLS_ConfigFileReader;

public class FileReaderManager {

	private static FileReaderManager fileReaderManager = new FileReaderManager();
	private static Credentials_ConfigFileReader credentials_ConfigFileReader;
	private static URLS_ConfigFileReader uRLS_ConfigFileReader;

	private FileReaderManager() {
	}

	public static FileReaderManager getInstance() {
		return fileReaderManager;
	}

	public Credentials_ConfigFileReader getCredentials_ConfigReader() {
		return (credentials_ConfigFileReader == null) ? new Credentials_ConfigFileReader()
				: credentials_ConfigFileReader;
	}

	public URLS_ConfigFileReader getURLS_ConfigReader() {
		return (uRLS_ConfigFileReader == null) ? new URLS_ConfigFileReader() : uRLS_ConfigFileReader;
	}

}
