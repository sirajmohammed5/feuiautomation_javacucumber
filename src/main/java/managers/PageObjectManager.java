package managers;

import org.openqa.selenium.WebDriver;

import pageObjects.AddFundsWithCreditCardPage;
import pageObjects.CompanySignUpPage;


public class PageObjectManager {

    private WebDriver driver;
    private CompanySignUpPage companySignUpPage;
    private AddFundsWithCreditCardPage addFundsWithCreditCardPage;

    public PageObjectManager(WebDriver driver){
        this.driver = driver;
    }

    public CompanySignUpPage getCompanySignUpPage(){
        return (companySignUpPage == null) ? companySignUpPage = new CompanySignUpPage(driver) : companySignUpPage;
    }

    public AddFundsWithCreditCardPage getAddFundsWithCreditCardPage(){
        return (addFundsWithCreditCardPage == null) ? addFundsWithCreditCardPage = new AddFundsWithCreditCardPage(driver) : addFundsWithCreditCardPage;
    }


}
