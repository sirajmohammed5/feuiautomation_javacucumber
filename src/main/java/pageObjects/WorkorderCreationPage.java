package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WorkorderCreationPage {

    public WorkorderCreationPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    // Skip Intro
    @FindBy(xpath = "//div[@id='skipIntro']")
    WebElement skipIntro;

    public void SkipIntro() {
        skipIntro.click();
    }

}
