package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddFundsWithCreditCardPage extends CompanyLoginPage {

    public AddFundsWithCreditCardPage(WebDriver driver) {
        super(driver);
    }

    // Funds tab in the Dashboard
    @FindBy(xpath = "//span[contains(text(),'Funds')]")
    WebElement fundsTabInDashboardWebElement;

    // Add Funds buttton
    @FindBy(xpath = "//span[contains(text(),'Add Funds')]")
    WebElement addFundsButtonWebElement;

    // Credit Card Radio Button
    @FindBy(xpath = "//mat-radio-group//mat-radio-button[@id='mat-radio-2']") 
    WebElement creditCardRadioButtonWebElement;

    // Credit card number
    @FindBy(xpath = "//div[@class='group ng-star-inserted']//div[@class='__PrivateStripeElement']//iframe")
    WebElement creditCardNumberWebElement;

    // Credit card expiry month and year
    @FindBy(xpath = "//div[@class='group ng-star-inserted']//div[@class='__PrivateStripeElement']//iframe")
    WebElement creditCardExpiryMonthAndYearWebElement;

    // Credit card CVC
    @FindBy(xpath = "//div[@class='group ng-star-inserted']//div[@class='__PrivateStripeElement']//iframe")
    WebElement creditCardCVCWebElement;

    // Credit card Zip code
    @FindBy(xpath = "//div[@class='group ng-star-inserted']//div[@class='__PrivateStripeElement']//iframe")
    WebElement creditCardZipCodeWebElement;

    // Amount to be added
    @FindBy(xpath = "//input[@ng-reflect-name='amount']")
    WebElement amountToBeAddedFromCreditCardWebElement;

    // Remember for future use check box
    @FindBy(xpath = "//mat-radio-group//div[@class='col-md-12']//mat-checkbox//div")
    WebElement rememberForFutureUseCheckBoxWebElement;

    // Submit Credit Card Payment Button
    @FindBy(xpath = "//span[contains(text(),' Submit Payment ')]")
    WebElement submitCreditCardPaymentWebElement;

    // Success message after adding funds via credit card
    @FindBy(xpath = "//span[contains(text(),' Submit Payment ')]")
    WebElement successMessageAfterAddingFundsWebElement;

    public void clickFundsTabInDashboard() {
        fundsTabInDashboardWebElement.click();
    }

    public void clickAddFundsButton() {
        addFundsButtonWebElement.click();
    }

    public boolean isCreditCardRadioButtonSelected() {

        boolean a = creditCardRadioButtonWebElement.isSelected();
        return a;
    }

    public void enterCreditCardNumber(String creditCardNumber) {
        creditCardNumberWebElement.sendKeys(creditCardNumber);

    }

    public void enterCreditCardExpiryDateAndMonth(String monthAndYear) {
        creditCardExpiryMonthAndYearWebElement.sendKeys(monthAndYear);

    }

    public void enterCreditCardCVCNumber(String cvc) {
        creditCardCVCWebElement.sendKeys(cvc);

    }

    public void enterCreditCardZipCode(String zipCode) {
        creditCardZipCodeWebElement.sendKeys(zipCode);

    }

    public void enterAmountToBeAddedToAccount(String amount) {
        amountToBeAddedFromCreditCardWebElement.sendKeys(amount);

    }

    public boolean isRemeberForFutureUseCheckBoxSelected() {
        boolean result = rememberForFutureUseCheckBoxWebElement.isSelected();
        return result;

    }

    public void clickSubmitCreditCardPayment() {
        submitCreditCardPaymentWebElement.click();
    }

    public void isSuccessMessageAfterAddingFundsDisplayed() {
        successMessageAfterAddingFundsWebElement.isDisplayed();
    }

    public boolean isSubmitCreditCardPaymentButtonEnabled() {
        boolean result = successMessageAfterAddingFundsWebElement.isEnabled();
        return result;
    }

}
