package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CompanySignUpPage {

    WebDriver driver;

    public CompanySignUpPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // Skip Intro
    @FindBy(xpath = "//div[@id='skipIntro']")
    WebElement skipIntro;

    // Skip Intro Text
    @FindBy(xpath = "//div[@id='skipIntro']")
    WebElement skipIntroText;

    // Company SignUp Link Text
    @FindBy(xpath = "//div[@class='header-btns']//a[contains(text(),'Hire an Engineer')]")
    WebElement companySignUpLinkText;

    // First Name
    @FindBy(xpath = "//input[@id='f-name']")
    WebElement firstName;

    // Last Name
    @FindBy(xpath = "//input[@id='l-name']")
    WebElement lastName;

    // Email
    @FindBy(xpath = "//input[@id='email']")
    WebElement email;

    // Contact Number
    @FindBy(xpath = "//input[@name='b_phone'][@id='b-phone']")
    WebElement contactNumber;

    // Company Name
    @FindBy(xpath = "//div[@class='form-control-wrap-2']/child::input[@id='b-name']")
    WebElement companyName;

    // Password
    @FindBy(xpath = "//input[@id='password']")
    WebElement password;

    // ReType Password
    @FindBy(xpath = "//input[@id='r-password']")
    WebElement reTypePassword;

    // Sign up Check Box
    @FindBy(xpath = "//input[@id='signUbcheckbox']")
    WebElement acceptTermsAndConditions;

    // Sign up Button Is Enabled
    @FindBy(xpath = "//input[@id='signup-button']")
    WebElement signUpButton_isEnabled;

    // Sign up Button Click
    @FindBy(xpath = "//input[@id='signup-button']")
    WebElement signUpButtonClick;

    // Dashboard Text
    @FindBy(xpath = "//strong[contains(text(), 'Let’s get started to setup your account and post the first job')]")
    WebElement dashboardTextAfterSignup;

    // Accept Cookies
    @FindBy(xpath = "/html/body/div[5]/div[2]/a")
    WebElement acceptCookies;

    // Sign up Verification Code
    @FindBy(xpath = "//input[@placeholder='Verification Code']")
    WebElement verificationCode;

    // Email Verified Text Element for Assertion
    @FindBy(xpath = "//h3[contains(text(),'Email verified')]")
    WebElement emailVerifiedTextElement;

    // click To Confirm Location In Signup Process
    @FindBy(xpath = "//span[contains(text(),'Confirm')]")
    WebElement confirmLocationButtonEnabled;

    // click To Confirm Location In Signup Process
    @FindBy(xpath = "//span[contains(text(),'Confirm')]")
    WebElement clickConfirmLocation;

    // Sign up Location
    @FindBy(xpath = "//input[@placeholder='Location']")
    WebElement signupLocation;

    // Sign up Location, Selecting Address from the dropdown Action(Down Key)
    @FindBy(xpath = "//input[@placeholder='Location']")
    WebElement signupLocationDownKey;

    // Sign up Location, Selecting Address Action(Enter Key)
    @FindBy(xpath = "//input[@placeholder='Location']")
    WebElement signupLocationEnterKey;

    // Submit Button
    @FindBy(xpath = "//span[contains(text(),'Submit')]")
    WebElement verificationSubmitButton;

    // Address Confirm Button Click
    @FindBy(xpath = "//span[contains(text(),'Confirm')]")
    WebElement addressConfirmButtonIsEnabled;

    // Address Confirm Button Click
    @FindBy(xpath = "//span[contains(text(),'Confirm')]")
    WebElement addressConfirmButtonClick;

    // GetStarted Button Is Enabled to Post the Workorders
    @FindBy(xpath = "//span[contains(text(),'Get Started')]")
    WebElement jobPostGetStartedButtonIsEnabled;

    // Dashboard Text Element Locator
    public String dashboardTextAfterSignupElement = "//strong[contains(text(), 'Let’s get started to setup your account and post the first job')]";

    // Dashboard Validation Code text area element locator
    public String validationCodeTextElement = "//input[@id='mat-input-0']";

    public void SkipIntro() {
        skipIntro.click();
    }

    public String skipIntroText() {
        String a = skipIntroText.getText();
        return a;
    }

    public void clickOnCompanySignUpLinkText() {
        companySignUpLinkText.click();
    }

    public void firstName(String f_Name) {
        firstName.sendKeys(f_Name);
    }

    public void lastName(String l_Name) {
        lastName.sendKeys(l_Name);
    }

    public void email(String e_mail) {
        email.sendKeys(e_mail);
    }

    public void contactNumber(String contact_Number) {
        contactNumber.sendKeys(contact_Number);
    }

    public void companyName(String company_Name) {
        companyName.sendKeys(company_Name);
    }

    public void password(String f_pass) {
        password.sendKeys(f_pass);
    }

    public void reTypePassword(String r_Pass) {
        reTypePassword.sendKeys(r_Pass);
    }

    public void verificationCode(String code) {
        verificationCode.sendKeys(code);
    }

    public String emailVerifiedTextElement() {
        String text = emailVerifiedTextElement.getText();
        return text;
    }

    public boolean confirmLocationButtonEnabled() {
        boolean a = confirmLocationButtonEnabled.isEnabled();
        return a;
    }

    public void clickConfirmLocation() {
        clickConfirmLocation.click();
    }

    public void signupLocation(String location) {
        signupLocation.sendKeys(location);
    }

    public void signupLocationDownKey() {
        signupLocationDownKey.sendKeys(Keys.DOWN);
    }

    public void signupLocationEnterKey() {
        signupLocationEnterKey.sendKeys(Keys.RETURN);
    }

    public void acceptTermsAndConditions() {
        acceptTermsAndConditions.click();
    }

    public boolean signupButton_isEnabled() {
        boolean a = signUpButton_isEnabled.isEnabled();
        return a;
    }

    public void signupButtonClick() {
        signUpButtonClick.click();
    }

    public void acceptCookies() {
        acceptCookies.click();
    }

    public void verificationSubmitButton() {
        verificationSubmitButton.click();
    }

    public boolean addressConfirmButtonIsEnabled() {
        boolean a = addressConfirmButtonIsEnabled.isEnabled();
        return a;
    }

    public void addressConfirmButtonClick() {
        addressConfirmButtonClick.click();
    }

    public boolean jobPostGetStartedButtonIsEnabled() {
        boolean a = jobPostGetStartedButtonIsEnabled.isEnabled();
        return a;
    }

    public void fillDetails() {
        firstName("Siraj");
        lastName("Mohammed");
        contactNumber("7737654562");
        companyName("Corp.Inc");
        password("Flower123");
        reTypePassword("Flower123");

    }

}
