package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CompanyLoginPage {

    WebDriver driver;

    public CompanyLoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // email/username
    @FindBy(xpath = "//input[@formcontrolname='email']")
    WebElement email_UsernameWebElement;

    // password
    @FindBy(xpath = "//input[@formcontrolname='password']")
    WebElement passwordWebElement;

    // Login page text(The FE platform enables businesses to engage technicians
    // when, where and how they're needed.)
    @FindBy(xpath = "//div[@class='logo']//p")
    WebElement pageTextWebElement;

    // Login button
    @FindBy(xpath = "//button[@type='submit']")
    WebElement loginButtonWebElement;

    public void enterEmailOrUsername(String emailOrUsername) {
        email_UsernameWebElement.sendKeys(emailOrUsername);

    }

    public void enterPassword(String password) {
        passwordWebElement.sendKeys(password);

    }

    public boolean isLoginPageTextDisplayed() {
        boolean result = pageTextWebElement.isDisplayed();
        return result;

    }

    public boolean isLoginButtonEnabled() {
        boolean result = loginButtonWebElement.isEnabled();
        return result;

    }

    public void clickLoginButton() {
        loginButtonWebElement.click();

    }

}
