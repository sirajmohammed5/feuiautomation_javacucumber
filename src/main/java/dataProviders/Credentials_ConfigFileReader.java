package dataProviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import enums.DriverType;
import enums.EnvironmentType;

public class Credentials_ConfigFileReader {

    private Properties properties;
    private final String propertyFilePath = "configs/Credentials.properties";

    public Credentials_ConfigFileReader() {

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Credentials.properties not found at " + propertyFilePath);
        }
    }

    /*
     * #MySQL Database
     */

    public String getDB_URL() {
        String DB_URL = properties.getProperty("DB_URL");
        if (DB_URL != null)
            return DB_URL;
        else
            throw new RuntimeException("DB_URL not specified in the Credentials.properties file.");
    }

    public String getDB_UserName() {
        String DB_UserName = properties.getProperty("DB_USER");
        if (DB_UserName != null)
            return DB_UserName;
        else
            throw new RuntimeException("DB_UserName not specified in the Credentials.properties file.");
    }

    public String getDB_Password() {
        String DB_Password = properties.getProperty("DB_Password");
        if (DB_Password != null)
            return DB_Password;
        else
            throw new RuntimeException("DB_Password not specified in the Credentials.properties file.");
    }

    /*
     * #WebDriver
     */

    public String getDriverPath() {
        String driverPath = properties.getProperty("driverPath");
        if (driverPath != null)
            return driverPath;
        else
            throw new RuntimeException(
                    "Driver Path not specified in the Credentials.properties file for the Key:driverPath");
    }

    public long getImplicitlyWait() {
        String implicitlyWait = properties.getProperty("implicitlyWait");
        if (implicitlyWait != null) {
            try {
                return Long.parseLong(implicitlyWait);
            } catch (NumberFormatException e) {
                throw new RuntimeException("Not able to parse value : " + implicitlyWait + " in to Long");
            }
        }
        return 30;
    }

    public String getApplicationUrl() {
        String url = properties.getProperty("url");
        if (url != null)
            return url;
        else
            throw new RuntimeException(
                    "Application Url not specified in the Credentials.properties file for the Key:url");
    }

    public DriverType getBrowser() {
        String browserName = properties.getProperty("browser");
        if (browserName == null || browserName.equals("chrome"))
            return DriverType.CHROME;
        else if (browserName.equalsIgnoreCase("firefox"))
            return DriverType.FIREFOX;
        else if (browserName.equals("iexplorer"))
            return DriverType.INTERNETEXPLORER;
        else
            throw new RuntimeException(
                    "Browser Name Key value in Credentials.properties is not matched : " + browserName);
    }

    public EnvironmentType getEnvironment() {
        String environmentName = properties.getProperty("environment");
        if (environmentName == null || environmentName.equalsIgnoreCase("local"))
            return EnvironmentType.LOCAL;
        else if (environmentName.equals("remote"))
            return EnvironmentType.REMOTE;
        else
            throw new RuntimeException(
                    "Environment Type Key value in Credentials.properties is not matched : " + environmentName);
    }

    public Boolean getBrowserWindowSize() {
        String windowSize = properties.getProperty("windowMaximize");
        if (windowSize != null)
            return Boolean.valueOf(windowSize);
        return true;
    }

    /* 
     * extent Reports 
     */

    public String getReportConfigPath(){
        String reportConfigPath = properties.getProperty("reportConfigPath");
        if(reportConfigPath!= null) return reportConfigPath;
        else throw new RuntimeException("Report Config Path not specified in the Credentials.properties file for the Key:reportConfigPath");		
    }
}