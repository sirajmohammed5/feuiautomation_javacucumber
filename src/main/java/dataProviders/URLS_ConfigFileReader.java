package dataProviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class URLS_ConfigFileReader {

    private Properties properties;
    private final String propertyFilePath = "configs/URLS_Configurations.properties";

    public URLS_ConfigFileReader() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }

    public String getFEPublicPageURL() {
        String publicPageUrl = properties.getProperty("publicPageUrl");
        if (publicPageUrl != null)
            return publicPageUrl;
        else
            throw new RuntimeException("publicPageUrl not specified in the Configuration.properties file.");
    }

    public String getCompanySignUpURL() {
        String companySignUpURL = properties.getProperty("companySignUpURL");
        if (companySignUpURL != null)
            return companySignUpURL;
        else
            throw new RuntimeException("companySignUpURL not specified in the Configuration.properties file.");
    }

    public String getCompanyDashboardURL() {
        String companyDashboardURL = properties.getProperty("companyDashboardURL");
        if (companyDashboardURL != null)
            return companyDashboardURL;
        else
            throw new RuntimeException("companyDashboardURL not specified in the Configuration.properties file.");
    }

    public String getCompanyFundsPageURL() {
        String companyFundsPageURL = properties.getProperty("companyFundsPageURL");
        if (companyFundsPageURL != null)
            return companyFundsPageURL;
        else
            throw new RuntimeException("companyFundsPageURL not specified in the Configuration.properties file.");
    }

    public String getCompanyAddFundsPageURL() {
        String companyAddFundsPageURL = properties.getProperty("companyAddFundsPageURL");
        if (companyAddFundsPageURL != null)
            return companyAddFundsPageURL;
        else
            throw new RuntimeException("companyAddFundsPageURL not specified in the Configuration.properties file.");
    }

    public String getCompanyLoginPageURL() {
        String companyLoginPageURL = properties.getProperty("companyLoginPageURL");
        if (companyLoginPageURL != null)
            return companyLoginPageURL;
        else
            throw new RuntimeException("companyLoginPageURL not specified in the Configuration.properties file.");
    }

}
