Feature: Adding Funds with Credit card
              Description: Adding funds to an account with the Blockscore Credit Card test details


        Scenario: Adding funds in the account with the valid stripe test credit card details
            Given user is on the FE login page
             When clicks login button providing email and password
             Then user should be redirected to the dashboard screen
              And clicks on the Funds tab from the left navigation panel
             Then user should be redirected to the funds transaction history page
              And clicks on the Add Funds button
             Then user should be navigated to the add funds page
              And user selects the credit card option to draw amount from
              And provides the valid credit card details and saves for the future use
              And clicks on the Submit Payment
             Then amount should be added to the cash section of the account
