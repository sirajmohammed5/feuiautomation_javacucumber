package testRunners;

import java.io.File;

import com.cucumber.listener.Reporter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import managers.FileReaderManager;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/SanityTests", glue = { "stepDefinition" }, plugin = {
        "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" }, monochrome = true)

public class TestRunner {

    @BeforeClass
    public static void setupBeforeClass() {
        // WebDriverManager.chromedriver().setup();
        // Reporter.assignAuthor("Siraj Mohammed");
    }

    @AfterClass
    public static void writeExtentReport() {
        Reporter.loadXMLConfig(
                new File(FileReaderManager.getInstance().getCredentials_ConfigReader().getReportConfigPath()));

        Reporter.setSystemInfo("Application Name", "Field Engineer");
        Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
        Reporter.setSystemInfo("Operating System Type", System.getProperty("os.name").toString());
        Reporter.setSystemInfo("Environment", "Test");
        Reporter.setSystemInfo("Java Version", "1.8.0_181");
        Reporter.setSystemInfo("Selenium Version", "3.141.59");
        Reporter.setSystemInfo("Cucumber Version", "1.2.5");
        Reporter.setTestRunnerOutput("Test Execution Cucumber Report");
    }

}
