package stepDefinition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import managers.FileReaderManager;
import managers.PageObjectManager;
import managers.WebDriverManager;
import pageObjects.CompanySignUpPage;
import utilities.MysqlDbConnection;
import utilities.UtilityFunctions;

public class CompanySignUpTest {

    private static final Logger logger = LogManager.getLogger(CompanySignUpTest.class);

    WebDriver driver;
    CompanySignUpPage companySignUpPage;
    PageObjectManager pageObjectManager;
    WebDriverManager webDriverManager;

    String email = UtilityFunctions.email;

    @Given("^user is on the fe public page$")
    public void user_is_on_fe_public_page() {

        webDriverManager = new WebDriverManager();
        driver = webDriverManager.getDriver();
        pageObjectManager = new PageObjectManager(driver);
        companySignUpPage = pageObjectManager.getCompanySignUpPage();

        driver.get(FileReaderManager.getInstance().getURLS_ConfigReader().getFEPublicPageURL());

        // assertEquals(companySignUpPage.skipIntroText(),"Skip Intro");
        // companySignUpPage.SkipIntro();
        companySignUpPage.acceptCookies();
        logger.info("Successfully reached the FE public page");
    }

    @When("^clicks on the hire an engineer textlink$")
    public void clicks_on_hire_an_engineer_textlink() throws InterruptedException {

        driver.get(FileReaderManager.getInstance().getURLS_ConfigReader().getCompanySignUpURL());

        assertEquals(FileReaderManager.getInstance().getURLS_ConfigReader().getCompanySignUpURL(),
                driver.getCurrentUrl());

        logger.info("Successfully reached the company sign-up page");
    }

    @And("^provide all the valid details$")
    public void provides_all_the_valid_details() {
        companySignUpPage.firstName("Siraj");
        companySignUpPage.lastName("Mohammed");
        companySignUpPage.email(email);
        System.out.println("The email used in the signup form: " + email);
        companySignUpPage.contactNumber("7738652670");
        companySignUpPage.companyName("externetworks Inc.");
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 250)");
        companySignUpPage.password("Flower123");
        companySignUpPage.reTypePassword("Flower123");
        logger.info("Entered all the valid details");
    }

    @And("^clicks the signup to Start Hiring button$")
    public void clicks_the_signup_to_start_hiring_button() throws InterruptedException {
        assertTrue(companySignUpPage.signupButton_isEnabled());
        companySignUpPage.signupButtonClick();
        logger.info("PASSED");
    }

    @Then("^It should be redirected to the company profile dashboard$")
    public void it_should_be_redirected_to_the_company_profile_dashboard() throws InterruptedException {
        // companySignUpPage.acceptTermsAndConditions();
        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath(companySignUpPage.dashboardTextAfterSignupElement)));

        assertEquals(FileReaderManager.getInstance().getURLS_ConfigReader().getCompanyDashboardURL(),
                driver.getCurrentUrl());

        logger.info("Successfully filled the sign-up form and clicked on the sign-up button");
    }

    @And("^verify the account by providing the email verification code$")
    public void verify_the_account_by_providing_the_email_verification_code() throws InterruptedException {

        MysqlDbConnection mysqlDbConnection = new MysqlDbConnection();
        int a = mysqlDbConnection.getValidationCode(email);
        String code = Integer.toString(a);
        System.out.println("The Email used in the code verification is : " + email);
        companySignUpPage.verificationCode(code);
        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath(companySignUpPage.validationCodeTextElement)));
        companySignUpPage.verificationSubmitButton();

    }

    @And("^confirms the Business location$")
    public void confirms_the_business_location() throws InterruptedException {

        if (companySignUpPage.addressConfirmButtonIsEnabled() == true) {

            companySignUpPage.addressConfirmButtonClick();

        } else {

            companySignUpPage.signupLocation("6322 N Mozart street chicago");
            driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
            companySignUpPage.signupLocationDownKey();
            companySignUpPage.signupLocationEnterKey();
            driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
            companySignUpPage.addressConfirmButtonClick();

        }

        logger.info("Successfully updated the address");
    }

    @Then("^the user should be able to create a workorder$")
    public void the_user_should_be_able_to_create_a_workorder() throws InterruptedException {
        assertTrue(companySignUpPage.jobPostGetStartedButtonIsEnabled());
        driver.quit();
        logger.info("get started button is enabled");
    }

}
