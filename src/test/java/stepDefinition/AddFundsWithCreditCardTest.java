package stepDefinition;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import managers.FileReaderManager;
import managers.PageObjectManager;
import managers.WebDriverManager;
import pageObjects.AddFundsWithCreditCardPage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AddFundsWithCreditCardTest {

    private static final Logger logger = LogManager.getLogger(AddFundsWithCreditCardTest.class);

    WebDriver driver;
    AddFundsWithCreditCardPage addFundsWithCreditCardPage;
    PageObjectManager pageObjectManager;
    WebDriverManager webDriverManager;

    @Given("^user is on the FE login page$")
    public void user_is_on_the_FE_login_page() {

        webDriverManager = new WebDriverManager();
        driver = webDriverManager.getDriver();
        pageObjectManager = new PageObjectManager(driver);
        addFundsWithCreditCardPage = pageObjectManager.getAddFundsWithCreditCardPage();
        driver.get(FileReaderManager.getInstance().getURLS_ConfigReader().getCompanyLoginPageURL());
        assertTrue(addFundsWithCreditCardPage.isLoginPageTextDisplayed());

    }

    @When("^clicks login button providing email and password$")
    public void clicks_login_button_providing_email_and_password() {
        CompanySignUpTest cSignUpTest = new CompanySignUpTest();
        String email = cSignUpTest.email;
        System.out.println("The Logged in email is: " + email);
        addFundsWithCreditCardPage.enterEmailOrUsername(email);
        // ---- addFundsWithCreditCardPage.enterEmailOrUsername("jan6@mailinator.com");
        addFundsWithCreditCardPage.enterPassword("Flower123");
        assertTrue(addFundsWithCreditCardPage.isLoginButtonEnabled());
        addFundsWithCreditCardPage.clickLoginButton();
        logger.info("Successfull");

    }

    @Then("^user should be redirected to the dashboard screen$")
    public void user_should_be_redirected_to_the_dashboard_screen() throws InterruptedException {
        Thread.sleep(2000);
        assertEquals(FileReaderManager.getInstance().getURLS_ConfigReader().getCompanyDashboardURL(),
                driver.getCurrentUrl());
    }

    @And("^clicks on the Funds tab from the left navigation panel$")
    public void clicks_on_the_Funds_tab_from_the_left_navigation_panel() {
        addFundsWithCreditCardPage.clickFundsTabInDashboard();

    }

    @Then("^user should be redirected to the funds transaction history page$")
    public void user_should_be_redirected_to_the_funds_transaction_history_page() throws InterruptedException {
        Thread.sleep(2000);
        assertEquals(FileReaderManager.getInstance().getURLS_ConfigReader().getCompanyFundsPageURL(),
                driver.getCurrentUrl());
    }

    @And("^clicks on the Add Funds button$")
    public void clicks_on_the_Add_Funds_button() {
        addFundsWithCreditCardPage.clickAddFundsButton();

    }

    @Then("^user should be navigated to the add funds page$")
    public void user_should_be_navigated_to_the_add_funds_page() throws InterruptedException {
        Thread.sleep(2000);
        assertEquals(FileReaderManager.getInstance().getURLS_ConfigReader().getCompanyAddFundsPageURL(),
                driver.getCurrentUrl());

    }

    @And("^user selects the credit card option to draw amount from$")
    public void user_selects_the_credit_card_option_to_draw_amount_from() throws InterruptedException {

        assertFalse(addFundsWithCreditCardPage.isCreditCardRadioButtonSelected());
    }

    @And("^provides the valid credit card details and saves for the future use$")
    public void provides_the_valid_credit_card_details_and_saves_for_the_future_use() throws InterruptedException {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 250)");
        addFundsWithCreditCardPage.enterCreditCardNumber("4111111111111111");
        addFundsWithCreditCardPage.enterCreditCardExpiryDateAndMonth("1025");
        addFundsWithCreditCardPage.enterCreditCardCVCNumber("123");
        addFundsWithCreditCardPage.enterCreditCardZipCode("12345");
        addFundsWithCreditCardPage.enterAmountToBeAddedToAccount("5000");
        assertFalse(addFundsWithCreditCardPage.isRemeberForFutureUseCheckBoxSelected());

    }

    @And("^clicks on the Submit Payment$")
    public void clicks_on_the_Submit_Payment() throws InterruptedException {

        assertTrue(addFundsWithCreditCardPage.isSubmitCreditCardPaymentButtonEnabled());
        addFundsWithCreditCardPage.clickSubmitCreditCardPayment();

    }

    @Then("^amount should be added to the cash section of the account$")
    public void amount_should_be_added_to_the_cash_section_of_the_account() throws InterruptedException {
        addFundsWithCreditCardPage.isSuccessMessageAfterAddingFundsDisplayed();
        Thread.sleep(2000);
        driver.quit();

    }

}
